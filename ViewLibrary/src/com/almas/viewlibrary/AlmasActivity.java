package com.almas.viewlibrary;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.*;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AlmasActivity extends Activity {

	public AlmasActivity() {

	}

	protected void showToast(String content, int duration) {
		Toast.makeText(this, content, duration).show();
	}

	@SuppressLint("ShowToast")
	protected void showToast(View view, String content, int duration) {
		Toast toast = Toast.makeText(this, content, duration);
		toast.setView(view);
		toast.show();
	}

	protected void setTextET(int id, String str) {
		getEditText(id).setText(str);
	}

	protected void setImage(int id, Bitmap bm) {
		getImageView(id).setImageBitmap(bm);
	}

	protected void setImage(int id, Uri uri) {
		getImageView(id).setImageURI(uri);

	}

	protected void setImage(int id, Matrix matrix) {
		getImageView(id).setImageMatrix(matrix);
	}

	protected void setImage(int id, Drawable drawable) {
		getImageView(id).setImageDrawable(drawable);
	}

	protected String getTextET(int id) {
		return getEditText(id).getText().toString();
	}

	protected void setTextTV(int id, String str) {
		getTextView(id).setText(str);
	}

	protected String getTextTV(int id) {
		return getTextView(id).getText().toString();
	}

	protected Button getButton(int id) {
		return (Button) getView(id);
	}

	protected EditText getEditText(int id) {
		return (EditText) getView(id);
	}

	protected TextView getTextView(int id) {
		return (TextView) getView(id);
	}

	protected ImageView getImageView(int id) {
		return (ImageView) getView(id);
	}

	protected View getView(int id) {
		return findViewById(id);
	}

}
